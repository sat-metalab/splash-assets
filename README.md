Splash assets
=============

This repository contains assets used for integration testing of Splash, as well as for testing the performances.

Structure
---------

The repository is structured as follows:

```
.
├── calibration_patterns: calibration patterns for testing automatic calibration
├── captured_patterns: calibration patterns as captured by a virtual camera
├── configurations: sample configuration, for testing features as well as performance
├── images: sample textures
├── models: sample 3D models
├── videos: sample videos of various encoding and resolutions
└── README.md
```

Licenses
--------

* Tornado: videos rendered from the Tornado Blender file which is part of the [Cosmos Laundromat](https://cloud.blender.org/p/cosmos-laundromat/) open film produced by the Blender Foundation. The file can be found [here](https://cloud.blender.org/films/cosmos-laundromat/?asset=1901), given that you have an account on the Blender Cloud. It is licensed under [CC-BY](https://creativecommons.org/licenses/by/4.0/).
* Mantissa VJ loops: all VJ loops the artist The Mantissa have been released in the public domain. You can find the source Blender files [here](https://mantissa.xyz/pages/vj.html).
